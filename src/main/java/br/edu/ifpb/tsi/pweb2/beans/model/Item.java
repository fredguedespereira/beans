package br.edu.ifpb.tsi.pweb2.beans.model;

import java.math.BigDecimal;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;

import org.springframework.format.annotation.NumberFormat;

import br.edu.ifpb.tsi.pweb2.beans.dto.PessoaItemDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NamedNativeQuery(name = "find_pessoa_item_not_sold", query = "select p.nome as ofertante, i.descricao as descricao, i.preco as preco "
                +
                "from pessoa p " +
                "inner join item i on i.id_pessoa = p.id " +
                "where i.vendido = false", resultSetMapping = "pessoa_item_dto")
@SqlResultSetMapping(name = "pessoa_item_dto", classes = @ConstructorResult(targetClass = PessoaItemDTO.class, columns = {
                @ColumnResult(name = "ofertante", type = String.class),
                @ColumnResult(name = "descricao", type = String.class),
                @ColumnResult(name = "preco", type = BigDecimal.class)
}))
@Data
@NoArgsConstructor
@AllArgsConstructor()
@Entity
public class Item {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Integer id;

        private String descricao;

        private String url;

        @NumberFormat(pattern = "###,##0.00")
        private BigDecimal preco;

        private Boolean vendido;

        @ManyToOne()
        @JoinColumn(name = "id_pessoa")
        private Pessoa pessoa;

}
