package br.edu.ifpb.tsi.pweb2.beans.controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifpb.tsi.pweb2.beans.model.Pessoa;
import br.edu.ifpb.tsi.pweb2.beans.model.User;
import br.edu.ifpb.tsi.pweb2.beans.repository.PessoaRepository;
import br.edu.ifpb.tsi.pweb2.beans.repository.UserRepository;

@Controller
@RequestMapping("/pessoas")
public class PessoaController {

    @Autowired
    PessoaRepository pessoaRepository;

    @Autowired
    UserRepository userRepository;

    @RequestMapping("/form")
    @PreAuthorize("hasRole('ADMIN')")
    public ModelAndView getForm(ModelAndView mav) {
        mav.setViewName("pessoas/form");
        mav.addObject("menu", "cadastrar");
        mav.addObject("titulo", "Nova pessoa");
        mav.addObject("pessoa", new Pessoa());
        return mav;
    }

    @RequestMapping(method = RequestMethod.GET)
    @PreAuthorize("hasRole('ADMIN')")
    public ModelAndView listAll(ModelAndView mav) {
        mav.setViewName("pessoas/list");
        mav.addObject("menu", "listar");
        mav.addObject("pessoas", pessoaRepository.findAll());
        return mav;
    }

    @RequestMapping(method = RequestMethod.POST)
    @Transactional
    @PreAuthorize("hasRole('ADMIN')")
    public ModelAndView save(@Valid Pessoa pessoa, BindingResult validation, ModelAndView mav,
            RedirectAttributes attr) {
        if (!validation.hasErrors()) {
            pessoaRepository.save(pessoa);
            mav.setViewName("redirect:pessoas");
            attr.addFlashAttribute("mensagem", pessoa.getNome() + " cadastrada(o) com sucesso!");
            attr.addFlashAttribute("pessoa", pessoa);
        } else {
            mav.addObject("mensagem", "Erros de validação! Corrija-os e tente novamente.");
            mav.setViewName("pessoas/form");
        }

        return mav;
    }

    @RequestMapping("/edit/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ModelAndView edit(@PathVariable("id") Integer id, ModelAndView mav, RedirectAttributes attr) {
        Pessoa p = null;
        Optional<Pessoa> optional = pessoaRepository.findById(id);
        if (optional.isPresent()) {
            p = optional.get();
            mav.addObject("pessoa", p);
            mav.addObject("titulo", "Atualizar cadastro");
            mav.setViewName("pessoas/form");
        } else {
            attr.addAttribute("mensagem", "Pessoa com id=" + id + " não encontrada!");
            mav.setViewName("redirect:pessoas");
        }
        return mav;
    }

    @RequestMapping("/rendimentos/{valor}")
    @PreAuthorize("hasRole('ADMIN')")
    public ModelAndView findRendimentosBiggerThan(@PathVariable("valor") BigDecimal valor, ModelAndView mav) {
        mav.setViewName("pessoas/list");
        mav.addObject("menu", "listar");
        mav.addObject("pessoas", pessoaRepository.findByRendimentosAnuaisGreaterThan(valor));
        return mav;
    }

    @RequestMapping("/itensvendidos")
    public ModelAndView sold(ModelAndView mav) {
        mav.addObject("pessoas", pessoaRepository.findAllHaveItensSold());
        mav.setViewName("pessoas/list");
        return mav;
    }

    @ModelAttribute("users")
    public List<User> getUserOptions() {
        return userRepository.findByEnabledTrue();
    }

}
