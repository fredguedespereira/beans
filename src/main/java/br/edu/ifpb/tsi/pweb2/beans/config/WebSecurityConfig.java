package br.edu.ifpb.tsi.pweb2.beans.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        // Temporariamente, apenas para criar de início estes 2 usuários
        // UserDetails userAdmin = User.builder()
        // .username("admin")
        // .password(encoder.encode("admin123"))
        // .roles("ADMIN")
        // .build();

        // UserDetails userMaria = User.builder()
        // .username("maria")
        // .password(encoder.encode("maria"))
        // .roles("USER")
        // .build();

        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .passwordEncoder(encoder);
        // .withUser(userAdmin)
        // .withUser(userMaria);

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/css/**", "/imagens/**", "/auth/users", "/acesso-negado")
                .permitAll()
                // .antMatchers("/pessoas/**").hasRole("ADMIN")
                .anyRequest()
                .authenticated()
                .and()
                .formLogin(form -> form
                        .loginPage("/auth")
                        .defaultSuccessUrl("/home", true)
                        .permitAll())
                .logout(logout -> logout.logoutUrl("/auth/logout"))
                .exceptionHandling().accessDeniedPage("/acesso-negado");
    }

}
