package br.edu.ifpb.tsi.pweb2.beans;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoBeansApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoBeansApplication.class, args);
	}

}
