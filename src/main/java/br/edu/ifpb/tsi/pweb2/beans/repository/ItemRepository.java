package br.edu.ifpb.tsi.pweb2.beans.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.edu.ifpb.tsi.pweb2.beans.dto.PessoaItemDTO;
import br.edu.ifpb.tsi.pweb2.beans.model.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item, Integer> {

    @Query(name = "find_pessoa_item_not_sold", nativeQuery = true)
    public List<PessoaItemDTO> findAllNotSold();

    @Query("from Item i join fetch i.pessoa p where p.user.username = :username")
    public List<Item> findByUsername(@Param("username") String username);

}
