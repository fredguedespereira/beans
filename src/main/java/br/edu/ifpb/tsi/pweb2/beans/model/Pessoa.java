package br.edu.ifpb.tsi.pweb2.beans.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.springframework.format.annotation.NumberFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

// Como deve ser um Java Bean
// * Ter um construtor sem argumentos
// * Ter métodos de acesso get's e set's para as propriedades da classe
// * Se o tipo for boolean, muda-se o "get" por "is"
// POJO = Plain Old Java Object
@Data
@NoArgsConstructor
@AllArgsConstructor()
@Entity
public class Pessoa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotEmpty(message = "O nome é valor obrigatório!")
    private String nome;

    @NotEmpty(message = "O endereço é valor obrigatório!")
    private String endereco;

    @Past(message = "Data deve ser passada")
    @NotNull(message = "Data de nascimento é valor obrigatório!")
    private LocalDate dataNascimento;

    @NumberFormat(pattern = "###,##0.00")
    @NotNull(message = "Os rendimentos anuais é valor obrigatório!")
    @DecimalMin(value = "100", message = "Valor mínimo é 100 reais")
    private BigDecimal rendimentosAnuais;

    @NumberFormat(pattern = "0.00")
    @DecimalMin(value = "1.0", message = "Altura mínima é 1 metro")
    @DecimalMax(value = "2.5", message = "Altura máxima é 2 metros")
    @NotNull(message = "Os rendimentos anuais é valor obrigatório!")
    private Float altura;

    private boolean ativo = true;

    @OneToMany(mappedBy = "pessoa")
    @ToString.Exclude
    private List<Item> itens;

    @OneToOne
    @JoinColumn(name = "username")
    private User user;

    public void addItem(Item item) {
        if (item != null) {
            this.itens.add(item);
            item.setPessoa(this);
        }
    }
}
