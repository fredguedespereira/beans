package br.edu.ifpb.tsi.pweb2.beans.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.edu.ifpb.tsi.pweb2.beans.model.Pessoa;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, Integer> {

    public List<Pessoa> findByRendimentosAnuaisGreaterThan(BigDecimal valor);

    @Query("from Pessoa p join fetch p.itens i where i.vendido = true")
    public List<Pessoa> findAllHaveItensSold();

    @Query("from Pessoa p join fetch p.user u where u.username = :username")
    public Pessoa findByUsername(@Param("username") String username);

}
