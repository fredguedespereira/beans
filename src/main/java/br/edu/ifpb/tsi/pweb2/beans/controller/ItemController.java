package br.edu.ifpb.tsi.pweb2.beans.controller;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifpb.tsi.pweb2.beans.model.Item;
import br.edu.ifpb.tsi.pweb2.beans.model.Pessoa;
import br.edu.ifpb.tsi.pweb2.beans.repository.ItemRepository;
import br.edu.ifpb.tsi.pweb2.beans.repository.PessoaRepository;

@Controller
@RequestMapping("/itens")
public class ItemController {

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private PessoaRepository pessoaRepository;

    @RequestMapping("/form")
    public ModelAndView getForm(ModelAndView mav) {
        mav.setViewName("itens/form");
        mav.addObject("menu", "cad-item");
        mav.addObject("titulo", "Novo item");
        mav.addObject("item", new Item());
        return mav;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView listAll(ModelAndView mav, Principal principal) {
        mav.setViewName("itens/list");
        mav.addObject("menu", "list-item");
        mav.addObject("itens", itemRepository.findByUsername(principal.getName()));
        return mav;
    }

    @RequestMapping(method = RequestMethod.POST)
    @Transactional
    public ModelAndView save(Item item, ModelAndView mav, RedirectAttributes attr, Principal principal) {
        Pessoa pessoa = pessoaRepository.findByUsername(principal.getName());
        pessoa.addItem(item);
        itemRepository.save(item);
        mav.setViewName("redirect:itens");
        attr.addFlashAttribute("mensagem", item.getDescricao() + " cadastrado com sucesso!");
        attr.addFlashAttribute("item", item);
        return mav;
    }

    @RequestMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable("id") Integer id, ModelAndView mav, RedirectAttributes attr) {
        Item item = null;
        Optional<Item> optional = itemRepository.findById(id);
        if (optional.isPresent()) {
            item = optional.get();
            mav.addObject("item", item);
            mav.addObject("titulo", "Atualizar cadastro de item");
            mav.setViewName("itens/form");
        } else {
            attr.addAttribute("mensagem", "Item com id=" + id + " não encontrado!");
            mav.setViewName("redirect:pessoas");
        }
        return mav;
    }

    @RequestMapping("/naovendidos")
    public ModelAndView notsold(ModelAndView mav) {
        mav.addObject("pessoasItens", itemRepository.findAllNotSold());
        mav.setViewName("itens/notsold");
        return mav;
    }

}
