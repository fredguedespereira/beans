package br.edu.ifpb.tsi.pweb2.beans.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.ifpb.tsi.pweb2.beans.model.User;

public interface UserRepository extends JpaRepository<User, String> {

    public List<User> findByEnabledTrue();

}
