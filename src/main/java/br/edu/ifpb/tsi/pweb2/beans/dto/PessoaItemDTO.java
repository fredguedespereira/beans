package br.edu.ifpb.tsi.pweb2.beans.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PessoaItemDTO {

    public String ofertante;

    private String descricao;

    private BigDecimal preco;

}
