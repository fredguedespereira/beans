package br.edu.ifpb.tsi.pweb2.beans.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.edu.ifpb.tsi.pweb2.beans.repository.UserRepository;

@Controller
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    UserRepository userRepository;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getForm(ModelAndView modelAndView) {
        modelAndView.setViewName("auth/login");
        return modelAndView;
    }

    @RequestMapping("/users")
    public ModelAndView getUsers(ModelAndView mav) {
        mav.addObject("users", userRepository.findAll());
        mav.setViewName("/auth/users");
        mav.addObject("menu", "list-users");
        return mav;
    }

}
