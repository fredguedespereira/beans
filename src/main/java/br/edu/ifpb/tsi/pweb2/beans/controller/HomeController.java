package br.edu.ifpb.tsi.pweb2.beans.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @RequestMapping("/home")
    public String getHome(Model m) {
        m.addAttribute("menu", "home");
        return "home";
    }

    @RequestMapping("/acesso-negado")
    public String getAcessoNegado(Model m) {
        m.addAttribute("menu", "home");
        return "/acesso-negado";
    }

}
